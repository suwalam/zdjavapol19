package pl.sda.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.sda.configuration.PersonConfiguration;
import pl.sda.model.Person;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;

public class SpringMain {

    public static void main(String[] args) {

        //Tworzymy instancję kontenera
        ApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(PersonConfiguration.class);

        PersonService personService = applicationContext.getBean("personServiceImpl", PersonServiceImpl.class);

        personService.add(new Person(1, "Jan", "Kowalski"));

        Person personById = personService.getById("1");

        System.out.println(personById);

    }
}
