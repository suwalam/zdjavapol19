package pl.sda.service;

import pl.sda.model.Person;

import java.util.List;


public interface PersonService {

    void add(Person person);

    Person getById(String id);

    List<Person> getAll();

}
