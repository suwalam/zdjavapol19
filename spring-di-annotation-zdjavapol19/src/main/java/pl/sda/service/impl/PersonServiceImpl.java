package pl.sda.service.impl;

import pl.sda.dao.PersonDao;
import pl.sda.model.Person;
import pl.sda.service.PersonService;
import pl.sda.util.PersonUtil;
import pl.sda.util.PersonValidator;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    private final PersonDao personDao;

    private final PersonUtil personUtil;

    private final PersonValidator personValidator;

    public PersonServiceImpl(PersonDao personDao, PersonUtil personUtil, PersonValidator personValidator) {
        this.personDao = personDao;
        this.personUtil = personUtil;
        this.personValidator = personValidator;
    }

    @Override
    public void add(Person person) {
        personValidator.validate(person);
        personDao.add(person);
    }

    @Override
    public Person getById(String id) {

        int parsedId = personUtil.parseIdToInt(id);

        return personDao.getById(parsedId);
    }

    @Override
    public List<Person> getAll() {
        return personDao.getAll();
    }
}
