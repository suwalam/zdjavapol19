package pl.sda.withdi;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PrimeNumberValidatorTest {

    @Test
    public void shouldReturnTrueForPrimeNumber() {
        //given
        final long primeNumber = 7;
        PrimeNumberValidator validator = new PrimeNumberValidator(new FixedNumberProvider(primeNumber));

        //when
        boolean isPrimeNumber = validator.isPrimeNumber();

        //then
        Assertions.assertTrue(isPrimeNumber);

    }

    @Test
    public void shouldReturnFalseForNonPrimeNumber() {
        //given
        final long nonPrimeNumber = 10;
        PrimeNumberValidator validator = new PrimeNumberValidator(new FixedNumberProvider(nonPrimeNumber));

        //when
        boolean isPrimeNumber = validator.isPrimeNumber();

        //then
        Assertions.assertFalse(isPrimeNumber);

    }
}