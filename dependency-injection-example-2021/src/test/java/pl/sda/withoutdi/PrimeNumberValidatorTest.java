package pl.sda.withoutdi;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PrimeNumberValidatorTest {

    @Test
    public void shouldReturnTrueForPrimeNumber() {
        //given
        PrimeNumberValidator validator = new PrimeNumberValidator();

        //when
        boolean isPrimeNumber = validator.isPrimeNumber();

        //then
        //?????
        Assertions.assertTrue(isPrimeNumber);

    }


}