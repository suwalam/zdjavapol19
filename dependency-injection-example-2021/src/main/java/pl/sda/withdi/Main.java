package pl.sda.withdi;


public class Main {
    public static void main(String[] args) {
        PrimeNumberValidator validator = new PrimeNumberValidator(new FixedNumberProvider(7L));

        System.out.println(validator.isPrimeNumber());
    }

}
