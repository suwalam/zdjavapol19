package pl.sda.withdi;

public class FixedNumberProvider implements NumberProvider {

    private final long fixedNumber;

    public FixedNumberProvider(long fixedNumber) {
        this.fixedNumber = fixedNumber;
    }

    @Override
    public long generateNumber() {
        System.out.println("Generated number: " + fixedNumber);
        return fixedNumber;
    }
}
