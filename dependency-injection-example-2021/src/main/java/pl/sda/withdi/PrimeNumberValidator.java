package pl.sda.withdi;

public class PrimeNumberValidator {

    private final NumberProvider numberProvider;


    public PrimeNumberValidator(NumberProvider numberProvider) {
        this.numberProvider = numberProvider;
    }

    public boolean isPrimeNumber() {

        long number = numberProvider.generateNumber();

        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }
}
