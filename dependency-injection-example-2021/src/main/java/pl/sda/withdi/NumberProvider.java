package pl.sda.withdi;

public interface NumberProvider {

    long generateNumber();
}
