package pl.sda.withdi;

import java.util.Random;

public class RandomNumberProvider implements NumberProvider{

    private static final Random RANDOM = new Random();

    @Override
    public long generateNumber() {
        long result =  RANDOM.nextLong();
        System.out.println("Random number: " + result);
        return result;
    }
}
