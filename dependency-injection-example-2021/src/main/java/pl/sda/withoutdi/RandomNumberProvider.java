package pl.sda.withoutdi;

import java.util.Random;

public class RandomNumberProvider {

    private static final Random RANDOM = new Random();

    public long generateNumber() {
        long result =  RANDOM.nextLong();
        System.out.println("Generated number: " + result);
        return result;
    }
}
