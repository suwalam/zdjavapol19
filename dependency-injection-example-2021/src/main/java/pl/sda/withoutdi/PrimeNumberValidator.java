package pl.sda.withoutdi;

public class PrimeNumberValidator {

    private final RandomNumberProvider numberProvider = new RandomNumberProvider();

    public boolean isPrimeNumber() {

        long number = numberProvider.generateNumber();

        for (long i = 2; i <= number / 2; ++i) {
            if (number % i == 0) {
                return true;
            }
        }

        return false;
    }
}
