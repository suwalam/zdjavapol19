package pl.sda.rest.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.sda.model.Book;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class BookRestConsumer implements CommandLineRunner {

    private RestTemplate restTemplate = new RestTemplate();

    private ObjectMapper objectMapper = new ObjectMapper();

    @PostConstruct
    public void init() {
        log.info("Start BookRestConsumer @PostConstruct");
    }

    @PreDestroy
    public void destroy() {
        log.info("Destroy BookRestConsumer @PreDestroy");
    }

    private void invokeGetAllBooks() throws JsonProcessingException {

        String getAllUrl = "http://localhost:8080/api/books/";

        String json = restTemplate.getForObject(getAllUrl, String.class);
        log.info("Received all Books: " + json);

        //Mapowanie JSONa na ArrayListę obiektów Book
        List<Book> Books = objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, Book.class));

        Books.forEach(b -> log.info(b.getTitle()));

    }

    private void invokeGetBookById() {
        String getByIdUrl = "http://localhost:8080/api/books/{id}/";

        Map<String, String> params = new HashMap<>();
        params.put("id", "8");

        Book book = restTemplate.getForObject(getByIdUrl, Book.class, params);
        log.info("Text Book for id 2: " + book.getTitle());
    }

    private void invokePostBook() {
        String postUrl = "http://localhost:8080/api/Books/";
        Book newBook = new Book(8, "New Title", "Jan Brzechwa", "4311", "desc", LocalDate.now());

        restTemplate.postForObject(postUrl, newBook, Book.class);
        log.info("Add Book: " + newBook);
    }


    @Override
    public void run(String... args) throws Exception {

//        invokeGetAllBooks();

//        invokeGetBookById();

//        invokePostBook();
//
//        invokeGetAllBooks();
//
        invokeGetBookById();
    }
}
