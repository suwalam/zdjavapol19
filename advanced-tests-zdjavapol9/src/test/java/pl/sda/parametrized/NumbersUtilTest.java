package pl.sda.parametrized;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumbersUtilTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 9, -5, 11, 23523523})
    public void shouldReturnTrueForOddNumbers(int input) {
        //when //then
        Assertions.assertThat(NumbersUtil.isOdd(input)).isTrue();
    }

    @ParameterizedTest
    @ValueSource(ints = {12, 34, 92, -50, 112, 235235236})
    public void shouldReturnFalseForEvenNumbers(int input) {
        //when //then
        Assertions.assertThat(NumbersUtil.isOdd(input)).isFalse();
    }

    @ParameterizedTest
    @MethodSource(value = "provideNumbersWithInfoAboutParity")
    public void shouldReturnExpectedValueForGivenInput(int input, boolean expected) {
        //when //then
        Assertions.assertThat(NumbersUtil.isOdd(input)).isEqualTo(expected);
    }

    private static Stream<Arguments> provideNumbersWithInfoAboutParity() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(12, false),
                Arguments.of(11, true),
                Arguments.of(-1, true),
                Arguments.of(16, false),
                Arguments.of(1643634, false)
        );
    }

    @Test
    public void shouldThrowIllegalArgumentException() {

        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> NumbersUtil.divide(12, 0))
                .withMessage("dividend can't be 0");
    }

}