package pl.sda.parametrized;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.*;

class StringsUtilTest {

    //to jest podejście zwykłe - jedna metoda - jeden przypadek testowy
    @Test
    public void shouldUpperCaseInput() {
        //given
        String input = "ala ma kota";
        String expected = "ALA MA KOTA";

        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        Assertions.assertThat(actual).isEqualTo(expected);
    }


    @ParameterizedTest
    //given
    @CsvSource(value = {"test;TEST", "    java;JAVA", " jaVA  ;JAVA", " test ;TEST"}, delimiter = ';')
    public void shouldTrimInputAndUpperCase(String input, String expected) {

        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        Assertions.assertThat(actual).isEqualTo(expected);

    }

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1, delimiter = ',', lineSeparator = ";")
    public void shouldTrimInputFromCSVAndUpperCase(String input, String expected) {
        //when
        String actual = StringsUtil.toUpperCase(input);

        //then
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void shouldBeBlankForNullAndEmptyInput(String input) {
        //when
        boolean actual = StringsUtil.isBlank(input);

        //then
        Assertions.assertThat(actual).isTrue();
    }

}