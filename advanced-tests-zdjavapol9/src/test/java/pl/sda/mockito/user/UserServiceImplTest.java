package pl.sda.mockito.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final long USER_ID = 1L;

    private static final User TEST_USER = new User(USER_ID, "Jan", "Kowalski");

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserValidator userValidator;

    @InjectMocks
    private  UserServiceImpl underTest;

/*    @BeforeEach //to podejście zastępuje użycie adnotacji @InjectMocks nad polem
    public void before() {
        underTest = new UserServiceImpl(userRepository, userValidator);
    }*/

    @Test
    public void shouldGetUserById() {
        //given
        when(userRepository.findById(USER_ID)).thenReturn(Optional.of(TEST_USER));

        //when
        User actual = underTest.getUserById(USER_ID);

        //then
        Assertions.assertThat(actual).isEqualTo(TEST_USER);
        verify(userRepository).findById(USER_ID);
        verifyNoInteractions(userValidator);
        verifyNoMoreInteractions(userRepository);
    }

    //stworzyć test ze ścieżką negatywną dla metody getUserById

    @Test
    public void shouldCreateUser() {

        //given
        when(userValidator.isUserValid(TEST_USER)).thenReturn(true);
        when(userRepository.addUser(TEST_USER)).thenReturn(TEST_USER);

        //when
        User actual = underTest.createUser(TEST_USER);

        //then
        Assertions.assertThat(actual).isEqualTo(TEST_USER);
        verify(userValidator).isUserValid(TEST_USER);
        verifyNoMoreInteractions(userValidator);
        verify(userRepository).addUser(TEST_USER);
        verifyNoMoreInteractions(userRepository);

    }

    //stworzyć test ze ścieżką negatywną dla metody createUser

}