package pl.sda.mockito.message;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PrivateMessageSenderTest {

    private static final String TEST_TEXT_MSG = "hello";

    private static final String TEST_AUTHOR_ID = "andrzej";

    private static final String TEST_RECIPIENT_ID = "marysia";

    @Mock
    private MessageProvider messageProvider;

    @Mock
    private MessageValidator messageValidator;

    @InjectMocks
    private PrivateMessageSender underTest;

    @Captor
    private ArgumentCaptor<Message> messageArgumentCaptor;

    @Test
    public void shouldSendPrivateMessage() {

        //given
        when(messageValidator.isMessageValid(any())).thenReturn(true);
        when(messageValidator.isMessageRecipientReachable(TEST_RECIPIENT_ID)).thenReturn(true);

        doNothing().when(messageProvider).send(any(), eq(MessageType.PRIVATE));

        //when
        underTest.sendPrivateMessage(TEST_TEXT_MSG, TEST_AUTHOR_ID, TEST_RECIPIENT_ID);

        //then
        verify(messageValidator).isMessageValid(any());
        verify(messageValidator).isMessageRecipientReachable(TEST_RECIPIENT_ID);
        verifyNoMoreInteractions(messageValidator);
        verify(messageProvider).send(any(), eq(MessageType.PRIVATE));
        verifyNoMoreInteractions(messageProvider);
    }

    @Test
    public void shouldSendPrivateMessageWithArgumentCaptor() {

        //given
        when(messageValidator.isMessageValid(any())).thenReturn(true);
        when(messageValidator.isMessageRecipientReachable(TEST_RECIPIENT_ID)).thenReturn(true);

        doNothing().when(messageProvider).send(any(), eq(MessageType.PRIVATE));

        //when
        underTest.sendPrivateMessage(TEST_TEXT_MSG, TEST_AUTHOR_ID, TEST_RECIPIENT_ID);

        //then
        verify(messageValidator).isMessageValid(messageArgumentCaptor.capture());
        verify(messageValidator).isMessageRecipientReachable(TEST_RECIPIENT_ID);
        verifyNoMoreInteractions(messageValidator);
        verify(messageProvider).send(messageArgumentCaptor.capture(), eq(MessageType.PRIVATE));
        verifyNoMoreInteractions(messageProvider);

        List<Message> allValues = messageArgumentCaptor.getAllValues();
        Assertions.assertThat(allValues.size()).isEqualTo(2);

        for (Message message : allValues) {
            Assertions.assertThat(message.getValue()).isEqualTo(TEST_TEXT_MSG);
            Assertions.assertThat(message.getAuthor()).isEqualTo(TEST_AUTHOR_ID);
            Assertions.assertThat(message.getRecipient()).isEqualTo(TEST_RECIPIENT_ID);
            Assertions.assertThat(message.getSendAt()).isBefore(LocalDateTime.now());
            Assertions.assertThat(message.getId()).isNotNull();
        }

    }
}