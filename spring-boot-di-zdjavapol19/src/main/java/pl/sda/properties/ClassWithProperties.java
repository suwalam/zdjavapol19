package pl.sda.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "pl.sda.collections")
@Component
public class ClassWithProperties {

    @Value("${pl.sda.login}")
    private String login;

    private List<String> roles; //kolekcja związana z kluczem pl.sda.collections.roles z plku application.properites

    private Map<String, String> map; //mapa związana z kluczem pl.sda.collections.map z plku application.properites

    public void printLogin() {
        System.out.println("Injected login from application.properites: " + login);
    }

    public void printRoles() {
        System.out.println("Injected collection roles from application.properites:");
        roles.forEach(System.out::println);
    }

    public void printMap() {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println("key: " + entry.getKey() + " value: " + entry.getValue());
        }
    }

    //setter wymagany jest do poprawnego wstrzyknięcia kolekcji roles z application.properites
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    //setter wymagany jest do poprawnego wstrzyknięcia kolekcji map z application.properites
    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}
