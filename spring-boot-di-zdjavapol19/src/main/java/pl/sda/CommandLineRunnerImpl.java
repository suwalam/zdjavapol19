package pl.sda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.sda.component.ComponentWithDependencies;
import pl.sda.component.SimpleLogger;
import pl.sda.cycle.ClassA;
import pl.sda.properties.ClassWithProperties;
import pl.sda.scope.RandomNumberReader1;
import pl.sda.scope.RandomNumberReader2;

@Component
public class CommandLineRunnerImpl implements CommandLineRunner {

    @Autowired
    private ComponentWithDependencies componentWithDependencies;

    @Qualifier("simpleSystemLogger")
    @Autowired
    private SimpleLogger simpleLogger;

    @Autowired
    private RandomNumberReader1 reader1;

    @Autowired
    private RandomNumberReader2 reader2;

    @Autowired
    private ClassA classA;

    @Autowired
    private ClassWithProperties classWithProperties;

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Hello Spring Boot!");

        componentWithDependencies.doSth();

        simpleLogger.printMessage("Message to log in console");

        reader1.printRandomNumber();

        reader2.printRandomNumber();

        classWithProperties.printLogin();

        classWithProperties.printRoles();

        classWithProperties.printMap();

    }
}
