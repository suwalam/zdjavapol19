package pl.sda.cycle;

import org.springframework.stereotype.Component;

@Component
public class ClassB {

    private final ClassC classC;


    public ClassB(ClassC classC) {
        this.classC = classC;
    }
}
