package pl.sda.cycle;

import org.springframework.stereotype.Component;

@Component
public class ClassC {

//    private final ClassA classA;
//
//    public ClassC(ClassA classA) {
//        this.classA = classA;
//    }

    //wstrzykujemy bean, który będzie zawierał to czego potrzebujemy z klasy A

    private final ClassD classD;

    public ClassC(ClassD classD) {
        this.classD = classD;
    }
}
