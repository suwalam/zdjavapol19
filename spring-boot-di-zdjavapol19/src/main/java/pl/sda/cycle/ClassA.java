package pl.sda.cycle;

import org.springframework.stereotype.Component;

@Component
public class ClassA {

    private final ClassB classB;


    public ClassA(ClassB classB) {
        this.classB = classB;
    }
}
