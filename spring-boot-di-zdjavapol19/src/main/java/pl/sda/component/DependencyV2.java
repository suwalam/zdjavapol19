package pl.sda.component;

import org.springframework.stereotype.Component;

@Component
public class DependencyV2 {

    public void doSth() {
        System.out.println("Do sth from Dependency V2");
    }

}
