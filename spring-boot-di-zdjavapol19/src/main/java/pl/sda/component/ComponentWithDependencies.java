package pl.sda.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ComponentWithDependencies {

    @Autowired //ta adnotacja jest wymagana do wstrzyknięcia obiektu Dependency
    private Dependency dependency;

    private DependencyV2 dependencyV2;

    @Autowired //ta adnotacja jest wymagana do wstrzyknięcia obiektu DependencyV2
    public void setDependencyV2(DependencyV2 dependencyV2) {
        this.dependencyV2 = dependencyV2;
    }

    public void doSth() {
        dependency.doSth();
        dependencyV2.doSth();
    }

}
