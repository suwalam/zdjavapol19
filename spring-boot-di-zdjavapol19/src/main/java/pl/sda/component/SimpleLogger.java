package pl.sda.component;

public interface SimpleLogger {

    void printMessage(String message);

}
