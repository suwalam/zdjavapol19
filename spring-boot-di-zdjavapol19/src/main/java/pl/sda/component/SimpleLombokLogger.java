package pl.sda.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SimpleLombokLogger implements SimpleLogger {

    @Override
    public void printMessage(String message) {
        log.info("LOG FROM SLF4J: " + message);
    }
}
