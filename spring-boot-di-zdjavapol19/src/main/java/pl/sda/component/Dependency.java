package pl.sda.component;

import org.springframework.stereotype.Component;

@Component
public class Dependency {

    public void doSth() {
        System.out.println("Do sth from Dependency");
    }

}
