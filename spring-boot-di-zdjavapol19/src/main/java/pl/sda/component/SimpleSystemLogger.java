package pl.sda.component;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class SimpleSystemLogger implements SimpleLogger {

    @Override
    public void printMessage(String message) {
        System.out.println("LOG FROM SYSTEM.OUT: " + message);
    }
}
