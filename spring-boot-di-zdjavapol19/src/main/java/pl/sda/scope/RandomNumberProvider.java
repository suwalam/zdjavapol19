package pl.sda.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Random;

@Scope("prototype")
@Component
public class RandomNumberProvider {

    private final int number = new Random().nextInt();

    public int getRandomValue() {
        return number;
    }
}
