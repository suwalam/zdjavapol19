package pl.sda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDiZdjavapol19Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDiZdjavapol19Application.class, args);
	}

}
