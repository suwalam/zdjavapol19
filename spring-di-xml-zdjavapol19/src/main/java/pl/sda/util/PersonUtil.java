package pl.sda.util;

public class PersonUtil {

    public int parseIdToInt(String id) {

        if (id == null || id.trim().isEmpty()) {
            throw new IllegalArgumentException("Id is empty");
        }

        return Integer.parseInt(id);

    }

}
