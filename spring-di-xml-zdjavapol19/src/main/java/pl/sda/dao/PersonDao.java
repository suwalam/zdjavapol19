package pl.sda.dao;

import pl.sda.model.Person;

import java.util.List;

public interface PersonDao {

    void add(Person person);

    Person getById(int id);

    List<Person> getAll();

}
