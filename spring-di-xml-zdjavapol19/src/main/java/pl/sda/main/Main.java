package pl.sda.main;

import pl.sda.dao.PersonDao;
import pl.sda.dao.impl.MockPersonDao;
import pl.sda.model.Person;
import pl.sda.service.PersonService;
import pl.sda.service.impl.PersonServiceImpl;
import pl.sda.util.PersonUtil;
import pl.sda.util.PersonValidator;

public class Main {

    public static void main(String[] args) {

        PersonDao personDao = new MockPersonDao();
        PersonUtil personUtil = new PersonUtil();
        PersonValidator personValidator = new PersonValidator();

        PersonService personService = new PersonServiceImpl(personDao, personUtil, personValidator);

        personService.add(new Person(1, "Jan", "Kowalski"));

        Person personById = personService.getById("1");

        System.out.println(personById);


    }

}
