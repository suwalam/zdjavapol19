package pl.sda.repository;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.sda.model.Book;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * More info about repository testing
 * https://reflectoring.io/spring-boot-data-jpa-test/
 */
@RunWith(SpringRunner.class)
@DataJpaTest
class BookRepositoryTest {


    @Autowired
    private BookRepository bookRepository;

    @Test
    public void shouldGetBookById() {
        //when
        Optional<Book> bookByIdOpt = bookRepository.findById(1);

        //then
        assertThat(bookByIdOpt.isPresent()).isTrue();
        Book bookById = bookByIdOpt.get();
        assertThat(bookById.getTitle()).isEqualTo("Python Test");
        assertThat(bookById.getAuthor()).isEqualTo("Jan Kowalski");
    }
}