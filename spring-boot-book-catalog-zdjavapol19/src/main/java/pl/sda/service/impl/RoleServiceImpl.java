package pl.sda.service.impl;

import org.springframework.stereotype.Service;
import pl.sda.model.Role;
import pl.sda.repository.RoleRepository;
import pl.sda.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }
}
