package pl.sda.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Book;
import pl.sda.repository.BookRepository;
import pl.sda.service.BookService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Transactional
@Primary
@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(final BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void save(Book book) {
        bookRepository.save(book);
    }

    @Override
    public Book getById(Integer id) {
        return bookRepository
                .findById(id)
                .orElse(null);
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    @Override
    public void update(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void delete(Integer id) {
        Book book = getById(id);

        if (book != null) {
            bookRepository.delete(book);
        }
    }

    @Override
    public List<Book> findByAuthor(String author) {
        List<Book> bookList = bookRepository.findByAuthor(author);



        return bookList == null ? new ArrayList<>() : bookList;
    }

    @Override
    public List<Book> findByTitle(String title) {
        List<Book> bookList = bookRepository.findByTitle(title);
        return bookList == null ? new ArrayList<>() : bookList;
    }

    @Override
    public List<Book> getAllSortedByTitle() {
        return getAll()
                .stream()
                .sorted(Comparator.comparing(Book::getTitle))
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> getAllSortedByReleaseDate() {
        return getAll()
                .stream()
                .sorted(Comparator.comparing(Book::getReleaseDate))
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> getAll(int pageNo, int pageSize, String sortBy) {

        //Przygotowanie informacji na temat numeru strony, ilości wierszy na pojedynczej stronie i opcjonalnie sortowania
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        //Metoda findAll jest dziedziczona z interfejsu JPARepository, który z kolei dziedziczy ją po PagingAndSortingRepository
        Page<Book> page = bookRepository.findAll(paging);

        //Z obiektu page pobieramy listę książek (wierszy) dla zadanych w obiekcie paging warunków
        List<Book> books = page.getContent();

        log.info("All books={}, totalElements={}, totalPages={}, PageNo={}, PageSize={}, sortBy={}",
                books.stream().map(b -> b.getId()).collect(Collectors.toList()),
                page.getTotalElements(), page.getTotalPages(), pageNo, pageSize, sortBy);

        return books;
    }
}
