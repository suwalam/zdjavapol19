package pl.sda.service;

public interface AutoLoginService {

    boolean autoLogin(String username, String password);
}
