package pl.sda.service;

import pl.sda.model.Book;

import java.util.List;

public interface BookService {

    void save(Book book);

    Book getById(Integer id);

    List<Book> getAll();

    void update(Book book);

    void delete(Integer id);

    List<Book> findByAuthor(String author);

    List<Book> findByTitle(String title);

    List<Book> getAllSortedByTitle();

    List<Book> getAllSortedByReleaseDate();

    List<Book> getAll(int pageNo, int pageSize, String sortBy);
}