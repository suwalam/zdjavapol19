package pl.sda.service;

import pl.sda.model.User;

public interface UserService {

    boolean existsByUsername(String username);

    boolean save(User user);

}
