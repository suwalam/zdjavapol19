package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 2, max=100, message = "Size must be at least 2 and max 100")
    private String title;

    @Size(min = 5, max=100, message = "Size must be at least 5 and max 100")
    private String author;

    private String isbn;

    @Column(length = 1000)
    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    //użycie tej adnotacji sprawi, że Spring automatycznie będzie formatował datę do zadanego szablonu yyyy-MM-dd
    private LocalDate releaseDate;

}
