package pl.sda.main;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordEncoderMain {

    public static void main(String[] args) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        System.out.println(encoder.encode("pass"));

        System.out.println(encoder.matches("pass", "$2a$10$2a8VIwIxwdGrvADkgTXv2OSmJi/tbm0ujpS8b3c90LNSoqqJxJmIm"));

    }
}
