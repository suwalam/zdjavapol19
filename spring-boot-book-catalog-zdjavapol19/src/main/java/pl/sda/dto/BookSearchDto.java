package pl.sda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Klasa, której obiekty transportują dane między stroną HTML a backendem Javy
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookSearchDto {

    private String author;

    private String title;

}
