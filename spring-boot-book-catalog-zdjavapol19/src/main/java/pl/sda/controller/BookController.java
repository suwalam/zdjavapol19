package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.sda.dto.BookSearchDto;
import pl.sda.model.Book;
import pl.sda.service.BookService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Slf4j
//@RequestMapping("/book")
@Controller
public class BookController {

    private final BookService bookService;

    public BookController(final BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/book/list")
    public String bookList(Model model) {
        model.addAttribute("books", bookService.getAll());

        //pytamy security context o nazwę obecnie zalogowanego użytkownika
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        model.addAttribute("currentUser", username);

        return "book-list";
    }

    /**
     *
     * @param model - model służący do przekazania danych na widok HTML
     * @param pageNo - numer żądanej strony - numerowany od zera!
     * @param pageSize - rozmiar strony - numerowany od jedynki!
     * @param sortBy - nazwa pola z encji Book, po którym chcemy sortować
     * @return - nazwa widoku
     */
    @GetMapping("/book/listparams")
    public String bookListWithParams(Model model,
                                     @RequestParam(defaultValue = "0") Integer pageNo,
                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                     @RequestParam(defaultValue = "id") String sortBy) {

        model.addAttribute("books", bookService.getAll(pageNo, pageSize, sortBy));

        //pytamy security context o nazwę obecnie zalogowanego użytkownika
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        model.addAttribute("currentUser", username);

        return "book-list";
    }

    @GetMapping("/book/detail/{bookId}")
    public String bookDetail(@PathVariable Integer bookId, Model model) {
        model.addAttribute("book", bookService.getById(bookId));
        return "book-detail";

    }

    @GetMapping("/book/delete/{bookId}")
    public String deleteBook(@PathVariable Integer bookId) {
        bookService.delete(bookId);
        log.info("Left " + bookService.getAll().size() + " books.");
        return "redirect:/book/list";
    }

    @GetMapping("/book/edit/{bookId}")
    public String editBook(@PathVariable Integer bookId, Model model) {
        model.addAttribute("book", bookService.getById(bookId));
        return "edit-book";
    }

    @PostMapping("/book/update")
    public String updateBook(@ModelAttribute Book book) {
        bookService.update(book);
        log.info("Updated book: " + book);
        return "redirect:/book/list";
    }

    @GetMapping("/admin/add")
    public String addBook(Model model) {
        model.addAttribute("book", new Book());
        return "add-book";
    }

    @PostMapping("/admin/save")
    public String saveBook(@Valid @ModelAttribute Book book, Errors errors) {

        if (errors.hasErrors()) {
            log.error("Error occured in front: " + errors.getFieldErrors());
            return "add-book"; //zwracamy widok add-book.html ze względu na błędy
        }

        bookService.save(book);
        log.info("Added book: " + book);
        return "redirect:/book/list";
    }

    @GetMapping("/book/search")
    public String search(Model model) {
        model.addAttribute("bookSearch", new BookSearchDto());
        return "book-search";
    }

    @PostMapping("/book/search-by-author")
    public String searchByAuthor(@ModelAttribute BookSearchDto bookSearchDto, Model model) {
        log.info("Searching by author: " + bookSearchDto.getAuthor());
        model.addAttribute("books", bookService.findByAuthor(bookSearchDto.getAuthor()));
        return "book-search-result";
    }

    @PostMapping("/book/search-by-title")
    public String searchByTitle(@ModelAttribute BookSearchDto bookSearchDto, Model model) {
        log.info("Searching by title: " + bookSearchDto.getTitle());
        model.addAttribute("books", bookService.findByTitle(bookSearchDto.getTitle()));
        return "book-search-result";
    }

    @GetMapping("/book/sort-by-title")
    public String bookListSortedByTitle(Model model) {
        model.addAttribute("books", bookService.getAllSortedByTitle());
        return "book-list";
    }

    @GetMapping("/book/sort-by-release-date")
    public String bookListSortedByReleaseDate(Model model) {
        model.addAttribute("books", bookService.getAllSortedByReleaseDate());
        return "book-list";
    }

}