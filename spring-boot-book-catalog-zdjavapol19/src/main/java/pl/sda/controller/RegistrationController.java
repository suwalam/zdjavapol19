package pl.sda.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.model.Role;
import pl.sda.model.User;
import pl.sda.service.AutoLoginService;
import pl.sda.service.RoleService;
import pl.sda.service.UserService;

import java.util.Arrays;

@Slf4j
@Controller
public class RegistrationController {

    private final UserService userService;

    private final RoleService roleService;

    private final AutoLoginService autoLoginService;

    public RegistrationController(UserService userService, RoleService roleService, AutoLoginService autoLoginService) {
        this.userService = userService;
        this.roleService = roleService;
        this.autoLoginService = autoLoginService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("user", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String registerNewUser(@ModelAttribute("user") User user) {

        //1. Username jest unikalny
        //2. Nowy użytkownik ma rolę USER
        //3. Automatyczne zalogowanie po poprawnej rejestracji


        if (userService.existsByUsername(user.getUsername())) {
            log.warn("User exists! " + user.getUsername());
            return "registration";
        }

        Role userRole = roleService.findByName("USER");

        user.setRoles(Arrays.asList(userRole));

        boolean isSuccess = userService.save(user);

        if (!isSuccess) {
            return "registration-error";
        }

        isSuccess = autoLoginService.autoLogin(user.getUsername(), user.getPassword());

        if (!isSuccess) {
            return "redirect:/login?error=true";
        }

        return "redirect:/book/list";
    }

}
