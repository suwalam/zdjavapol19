package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.model.Book;

import java.util.List;

/**
 * Zastosowałem JPARepository, kóre rozszerza CrudRepository i dysponuje większą ilością predefiniowanych metod
 */

public interface BookRepository extends JpaRepository<Book, Integer> {

    List<Book> findByAuthor(String author);

    @Query(value = "SELECT b FROM Book b WHERE b.title = :title")
    List<Book> findByTitle(@Param("title") String title);

}
